package ch.teko.kontaktdateapi.model

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.validation.constraints.NotBlank

@Entity
class Person(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        val id:Int = -1,

        @get: NotBlank
        val name:String = "",

        val alter:Int,

        @get: NotBlank
        val bemerkung:String = ""
)