package ch.teko.kontaktdateapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class KontaktdateApiApplication

fun main(args: Array<String>) {
	runApplication<KontaktdateApiApplication>(*args)
}
