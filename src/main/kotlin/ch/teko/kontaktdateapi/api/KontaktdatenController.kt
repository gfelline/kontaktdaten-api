package ch.teko.kontaktdateapi.api

import ch.teko.kontaktdateapi.model.Person
import ch.teko.kontaktdateapi.model.PersonRepository
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@RestController
@RequestMapping("/kontaktdaten-api")
class KontaktdatenController(val personRepository: PersonRepository) {

    @PostMapping("/person")
    fun createPerson(@Valid @RequestBody person: Person) : Person {
        val savedPerson = personRepository.save(person)
        return savedPerson
    }

    @GetMapping("/persons")
    fun getAllPersons() : List<Person> {
        return personRepository.findAll()
    }
}